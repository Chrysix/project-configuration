DROP TABLE vehicle;
CREATE TABLE vehicle(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    brand VARCHAR(255),
    model VARCHAR(255),
    color VARCHAR(255),
    weight DOUBLE,
    type VARCHAR(255),
    motorized BOOLEAN,
    purchase_date DATE,
    price_date DOUBLE,
    price_resale DOUBLE
);