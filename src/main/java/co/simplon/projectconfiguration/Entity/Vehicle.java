package co.simplon.projectconfiguration.Entity;

import java.time.LocalDate;

public class Vehicle {
    private Integer id;
    private String brand;
    private String model;
    private String color;
    private Double weight;
    private String type;
    private Boolean motorized;
    private LocalDate purchase_date;
    private Double price_date;
    private Double price_resale;

}
