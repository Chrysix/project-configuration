package co.simplon.projectconfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectConfigurationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectConfigurationApplication.class, args);
	}

}
